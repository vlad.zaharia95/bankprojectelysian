# Bank Project Elysian
   
   The program was made in Java NetBeans 8.2RC and Java SE Development Kit 8u281
   Bank specifications:
   - An account can only be created with a completely valid CNP. 
   - An account can only be created with at least 1000 RON or 1000EUR. Ex: 1000 RON, 0 EUR works fine.
   - You cannot withdraw if by doing so would be less than 1000 EUR/1000 RON. Ex: You can't withdraw 1 euro if you have 1000 eur. You also can't withdraw if you have 10 eur. But you can withdraw 1 euro if your balance is 1001 EUR.
   - An account cannot be terminated if there is still money on it.
   - In order to transfer all the money from one account you need a second client account which you can transfer all the money to the second client
   - A bank account can be monitorized by FISC.
   - All subsequent transaction since the moment of the monitorization will be saved by FISC.
   - Fisc will stop monitorizing the client of the client terminates account.
   
