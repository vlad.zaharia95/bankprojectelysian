/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian;

import bankprojectelysian.Accounts.Client;
import java.util.List;
import rocks.iulian.cnp.Cnp;

/**
 *
 * @author Windows 10
 */
public class Fisc {

    private static Fisc fiscInstance;
    private final FiscCallback fiscCallback;

    private Fisc(FiscCallback callback) {
        fiscCallback = callback;
    }

    public static Fisc getInstance(FiscCallback callback) {
        if (fiscInstance == null) {
            fiscInstance = new Fisc(callback);
        }
        return fiscInstance;
    }

    public Client AddMonitorizedClient(Cnp cnp) {
        return Bank.getInstance().SubscribeClientWithCNP(cnp);
    }

    public Client RemoveMonitorizedClient(Cnp cnp) {
        return Bank.getInstance().UnSubscribeClientWithCNP(cnp);
    }

    public List<Client> GetAllMonitorizedClients() {
        return Bank.getInstance().GetAllMonitorizedClients();
    }

    public void Update(Client client) {
        if (client != null) {
            System.out.println(client.GetLastTransaction().toString());
            fiscCallback.AppendMessage(client.GetLastTransaction().toString() + "\n");
        }

    }
}
