/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian.Accounts;

import bankprojectelysian.helperclasses.ResultCodes;
import java.io.Serializable;
import java.math.BigDecimal;
import org.joda.money.Money;

/**
 *
 * @author Windows 10
 */
public abstract class Account implements Serializable {

    protected Money currentAmount; //current currency amount in account (Joda Money JAR file)

    public Account(Money currentAmount) {//accounts can only be created with a minimum of 1000
        this.currentAmount = currentAmount;
    }

    public BigDecimal GetCurrentAmount() {
        return currentAmount.getAmount();
    }

    public String WithdrawMoney(BigDecimal amountToRemove) {
        BigDecimal tempAmount = currentAmount.minus(amountToRemove).getAmount();
        if (tempAmount.compareTo(BigDecimal.valueOf(1000)) == -1) {//Account goes <=1000 so transaction cannot be made
            return ResultCodes.ERR_WITHDRAW_BELOW1000;
        }
        currentAmount = currentAmount.minus(amountToRemove);
        return ResultCodes.OK_TRANSACTION;
    }

    public void DepositMoney(BigDecimal amountToDeposit) {
        currentAmount = currentAmount.plus(amountToDeposit);
    }

    public int SubtractMoney(BigDecimal amountToRemove) {
        BigDecimal tempAmount = currentAmount.minus(amountToRemove).getAmount();
        if (tempAmount.compareTo(BigDecimal.valueOf(0)) == -1) {//Account goes <0 so transaction cannot be made
            return -2;
        }
        currentAmount = currentAmount.minus(amountToRemove);
        return 1;
    }

    public void AddMoney(BigDecimal amountToDeposit) {
        currentAmount = currentAmount.plus(amountToDeposit.setScale(2, BigDecimal.ROUND_DOWN));
    }

    public abstract String GetAmountFormatted();
    public abstract String GetAccountType();
}
