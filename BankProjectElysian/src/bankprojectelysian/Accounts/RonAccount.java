/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian.Accounts;

import java.util.Locale;
import org.joda.money.Money;

/**
 *
 * @author Windows 10
 */
public class RonAccount extends Account {

    public RonAccount(Money currentAmount) {
        super(currentAmount);
    }

    @Override
    public String GetAmountFormatted() {
        Locale locale = new Locale("ro", "RO");
        return AccountFormatter.FormatCurrency(locale, currentAmount);
    }

    @Override
    public String GetAccountType() {
        return "RON Account";
    }

}
