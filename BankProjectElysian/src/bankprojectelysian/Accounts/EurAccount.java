/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian.Accounts;

import java.util.Locale;
import org.joda.money.Money;

/**
 *
 * @author Windows 10
 */
public class EurAccount extends Account {

    public EurAccount(Money currentAmount) {
        super(currentAmount);
    }

    @Override
    public String GetAmountFormatted() {
        return AccountFormatter.FormatCurrency(Locale.GERMANY, currentAmount);//Germany was the first country to use EURO
    }

    @Override
    public String GetAccountType() {
        return "EUR Account";
    }
}
