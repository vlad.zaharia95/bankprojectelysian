/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian.Accounts;

import bankprojectelysian.Accounts.Account;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import rocks.iulian.cnp.Cnp;

/**
 *
 * @author Windows 10
 */
public class Transaction implements Serializable {//helps with showing transactions at fisc

    //id?
    private TransactionTypes.Transaction type;
    private BigDecimal amount;
    private Cnp cnp;
    private HashMap<AccountTypes.Currency, Account> accounts;

    public Transaction(Cnp cnp, TransactionTypes.Transaction transactionType, BigDecimal amount, HashMap<AccountTypes.Currency, Account> accounts) {
        this.cnp = cnp;
        this.type = transactionType;
        this.amount = amount;
        this.accounts = accounts;
    }

    public String toString() {
        return cnp.toString() + " performs action: " + type + " with amount: " + amount
                + " EUR Account Balance: "
                + accounts.get(AccountTypes.Currency.EUR).GetCurrentAmount() + " RON Account Balance: "
                + accounts.get(AccountTypes.Currency.RON).GetCurrentAmount();
    }
}
