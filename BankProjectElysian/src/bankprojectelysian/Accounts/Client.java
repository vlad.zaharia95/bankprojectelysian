/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian.Accounts;

import bankprojectelysian.Accounts.Account;
import bankprojectelysian.Accounts.Transaction;
import bankprojectelysian.Accounts.AccountTypes.Currency;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import rocks.iulian.cnp.Cnp;

/**
 *
 * @author Windows 10
 */
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;//required for serialization?
    private Cnp cnp;
    private HashMap<Currency, Account> accounts;//Using hash-map here because the accounts are not necersarilly in an ordered list so a key-value map makes sense
    private List<Transaction> transactions;

    public Client(Cnp cnp, HashMap<Currency, Account> accounts) {
        this.cnp = cnp;
        this.accounts = accounts;
        transactions = new ArrayList<>();
    }

    public String Withdraw(BigDecimal withdrawAmount, Currency currency) {//TODO: Add monitorized message
        transactions.add(new Transaction(this.cnp, TransactionTypes.Transaction.WITHDRAW, withdrawAmount, accounts));
        String result = GetAccount(currency).WithdrawMoney(withdrawAmount);
        return result;

    }

    public void Deposit(BigDecimal depositAmount, Currency currency) {//TODO: Add monitorized message
        transactions.add(new Transaction(this.cnp, TransactionTypes.Transaction.DEPOSIT, depositAmount, accounts));
        GetAccount(currency).DepositMoney(depositAmount);
    }

    public BigDecimal GetCurrentAmount(Currency currency) {
        return GetAccount(currency).GetCurrentAmount();
    }

    public Transaction GetLastTransaction() {
        if(transactions.size()>0)
        return transactions.get(transactions.size() - 1);
        return null;
    }

    public Account GetAccount(Currency currency) {//gets account based on the type of currency 
        return accounts.get(currency);
    }

    public Cnp GetCnp() {
        return cnp;
    }

    public int TransferSubtract(AccountTypes.Currency currency, BigDecimal transferAmount, Cnp toCnp) {
        transactions.add(new Transaction(this.cnp, TransactionTypes.Transaction.TRANSFER, transferAmount, accounts));
        int result = GetAccount(currency).SubtractMoney(transferAmount);
        return result;//TODO: Add monitorized message
    }

    public void TransferAdd(AccountTypes.Currency currency, BigDecimal transferAmount, double conversionRate) {
        transferAmount = BigDecimal.valueOf(transferAmount.doubleValue() * conversionRate);
        GetAccount(currency).AddMoney(transferAmount);
    }
    
    public boolean HasNoMoney(){//is has no money on any account, the account can be terminated
        for (Map.Entry<Currency,Account> entry : accounts.entrySet()){
            if(entry.getValue().GetCurrentAmount().compareTo(BigDecimal.ZERO) != 0)
                return false;
        }    
        return true;
    }

    @Override
    public String toString() {
        return cnp.toString();
    }
}
