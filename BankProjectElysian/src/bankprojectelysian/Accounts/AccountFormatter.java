/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian.Accounts;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import org.joda.money.Money;

/**
 *
 * @author Windows 10
 */
public class AccountFormatter {//helps with displaying money in correct format

    public static String FormatCurrency(Locale icuLocale, Money amount) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance(icuLocale);
        String currency = formatter.format(amount.getAmount());
        System.out.println(currency + " for the locale " + icuLocale);
        return currency;
    }
}
