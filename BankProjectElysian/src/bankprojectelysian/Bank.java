/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian;

import bankprojectelysian.Accounts.Client;
import bankprojectelysian.Accounts.Account;
import bankprojectelysian.Accounts.EurAccount;
import bankprojectelysian.Accounts.RonAccount;
import bankprojectelysian.Accounts.AccountTypes;
import bankprojectelysian.helperclasses.ConversionRates;
import bankprojectelysian.helperclasses.FileIO;
import bankprojectelysian.helperclasses.ResultCodes;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.joda.money.Money;
import rocks.iulian.cnp.Cnp;
import rocks.iulian.cnp.RomanianPersonalNumber;

/**
 *
 * @author Windows 10
 */
public class Bank {

    private static Bank bankInstance;
    private List<Client> clients;
    private List<Client> monitorizedClients;

    private Bank() {
        //reads the files for normal and monitorized clients and adds into the list
        try {
            clients = FileIO.ReadBankObjects("myObjects.txt");
            monitorizedClients = FileIO.ReadBankObjects("myObjectsFisc.txt");
            for (int i = 0; i < monitorizedClients.size(); i++) {
                System.out.println(monitorizedClients.get(i).toString());
            }
            return;
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.toString());
        }
        clients = new ArrayList<>();
        monitorizedClients = new ArrayList<>(); //read from file else make new one
    }

    //singleton
    public static Bank getInstance() {
        if (bankInstance == null) {
            bankInstance = new Bank();
        }

        return bankInstance;
    }

    //adds client into list and saves file with new client added
    public String AddClient(Cnp cnp, Money initialEurDeposit, Money initialRonDeposit) {
        System.out.println(initialEurDeposit.getAmount() + " " + initialRonDeposit.getAmount());

        if (!ValidateClient(cnp, initialEurDeposit.getAmount(), initialRonDeposit.getAmount())) {
            //display error message
            return ResultCodes.ERR_MULTIPLE; //-1 means error
        }

        HashMap<AccountTypes.Currency, Account> hm = new HashMap();

        Account eurAccount = new EurAccount(initialEurDeposit);
        Account ronAccount = new RonAccount(initialRonDeposit);

        hm.put(AccountTypes.Currency.EUR, eurAccount);
        hm.put(AccountTypes.Currency.RON, ronAccount);

        Client client = new Client(cnp, hm);
        clients.add(client);
        System.out.println(client);
        FileIO.WriteBankFile(clients, "myObjects.txt");
        return ResultCodes.OK_CLIENT_ADD; //1 means ok
    }

    public List<Client> GetClients() {
        return clients;
    }

    //withdraw
    public String ClientWithdraw(Cnp cnp, BigDecimal withdrawAmount, AccountTypes.Currency currency) {
        Client client = null;
        String result = "";
        client = IsCnpValid(cnp);
        if (client != null) {
            if (withdrawAmount.compareTo(BigDecimal.ZERO) == 0) {
                return ResultCodes.ERR_WITHDRAW_ZERO;//can't be zero;
            }
            result = client.Withdraw(withdrawAmount, currency);
            FileIO.WriteBankFile(clients, "myObjects.txt");
            return result;
        } else {
            return ResultCodes.ERR_INVALID_CNP;
        }
    }

    //deposit
    public String ClientDeposit(Cnp cnp, BigDecimal depositAmount, AccountTypes.Currency currency) {
        Client client = null;
        client = IsCnpValid(cnp);

        if (client != null) {

            if (depositAmount.compareTo(BigDecimal.ZERO) == 0) {
                return ResultCodes.ERR_DEPOSIT_ZERO;//can't be zero;
            }
            client.Deposit(depositAmount, currency);
            FileIO.WriteBankFile(clients, "myObjects.txt");
            return ResultCodes.OK_TRANSACTION;//success
        } else {
            return ResultCodes.ERR_INVALID_CNP;
        }
    }

    //returns all normal clients
    public List<Client> GetAllClients() {
        return clients;
    }

    //returns all monitorized clients
    public List<Client> GetAllMonitorizedClients() {
        return monitorizedClients;
    }

    //returns specific client with cnp
    public Client GetClientWithCnp(Cnp cnp, ArrayList<Client> clients) {
        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).GetCnp().toString().equals(cnp.toString())) {
                return clients.get(i);
            }
        }
        return null;
    }

    //returns balance of all acounts
    public String CheckBalance(Cnp cnp) {
        Client client = null;
        client = IsCnpValid(cnp);
        if (client != null) {
            return client.GetCnp().toString() + "\n"
                    + "EUR Balance: " + client.GetAccount(AccountTypes.Currency.EUR).GetAmountFormatted() + "\n"
                    + "RON Balance: " + client.GetAccount(AccountTypes.Currency.RON).GetAmountFormatted();

        } else {
            return ResultCodes.ERR_INVALID_CNP;
        }
    }

    //helps to check if client is monitorized
    private boolean IsClientMonitorized(Client client) {
        for (int i = 0; i < monitorizedClients.size(); i++) {
            if (monitorizedClients.get(i).GetCnp().toString().equals(client.GetCnp().toString())) {
                return true;
            }
        }
        return false;
    }

    //validates all possible problems a client would have upon creation
    private boolean ValidateClient(Cnp cnp, BigDecimal initialEurDeposit, BigDecimal initialRonDeposit) {
        try {
            if (!RomanianPersonalNumber.isValid(cnp.stringify()
            )) {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }

        if (initialEurDeposit.compareTo(BigDecimal.valueOf(0)) == -1 || initialRonDeposit.compareTo(BigDecimal.valueOf(0)) == -1) {
            System.out.println("No Negative numbers");
            return false;
        }

        if (initialEurDeposit.compareTo(BigDecimal.valueOf(1000)) == -1 && initialRonDeposit.compareTo(BigDecimal.valueOf(1000)) == -1) {
            System.out.println("Not Enough Money For Deposit");
            return false;
        }
        if (GetClientWithCnp(cnp, (ArrayList) clients) != null) {
            System.out.println("Client Already Exists");
            return false;
        }
        return true;
    }

    //adds client to monitorized clients
    public Client SubscribeClientWithCNP(Cnp cnp) {
        if (monitorizedClients.contains(GetClientWithCnp(cnp, (ArrayList) monitorizedClients))) {//client exists
            return null;
        }
        Client client = GetClientWithCnp(cnp, (ArrayList) clients);
        monitorizedClients.add(client);
        return client;
    }

    //UNUSED but unsubscribes from fisc
    public Client UnSubscribeClientWithCNP(Cnp cnp) {
        Client client = GetClientWithCnp(cnp, (ArrayList) monitorizedClients);
        monitorizedClients.remove(client);
        return client;
    }

    //Transfer
    public String Transfer(Cnp fromCnp, Cnp toCnp, AccountTypes.Currency fromAccount,
            AccountTypes.Currency toAccount, BigDecimal transferAmount) {
        Client fromTransfer = null;
        Client toTransfer = null;

        fromTransfer = IsCnpValid(fromCnp);
        toTransfer = IsCnpValid(toCnp);
        if (fromTransfer != null && toTransfer != null) {
            if (transferAmount.compareTo(BigDecimal.ZERO) == 0) {
                return ResultCodes.ERR_TRANSFER_ZERO;
            }

            double conversionRate = DetermineConversionRate(fromAccount, toAccount);
            int result = fromTransfer.TransferSubtract(fromAccount, transferAmount, toTransfer.GetCnp());
            if (result != 1) {//no enough to transfer
                return ResultCodes.ERR_NOTENOUGH_TRANSFER;
            }
            toTransfer.TransferAdd(toAccount, transferAmount, conversionRate);

            FileIO.WriteBankFile(clients, "myObjects.txt");
            return ResultCodes.OK_TRANSACTION;
        } else {
            return ResultCodes.ERR_INVALID_CNP;
        }
    }

    //Determines the conversion rate
    public double DetermineConversionRate(AccountTypes.Currency from, AccountTypes.Currency to) {
        if (from.toString().equals(to.toString())) {
            return 1;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(from.toString()).append("to").append(to.toString());
        String key = builder.toString().toLowerCase();
        return ConversionRates.rates.get(key);
    }

    //closes accounts and updates the files
    public String CloseAccount(Cnp cnp) {
        Client client = null;
        client = IsCnpValid(cnp);
        if (client != null) {
            if (client.HasNoMoney()) {
                clients.remove(client);
                if (IsClientMonitorized(client)) {
                    RemoveMonitorizedClient(client.GetCnp());
                    FileIO.WriteBankFile(monitorizedClients, "myObjectsFisc.txt");
                }
                FileIO.WriteBankFile(clients, "myObjects.txt");
                return ResultCodes.OK_ACCOUNT_TERMINATED;
            } else {
                return ResultCodes.ERR_ACCOUNT_HASMONEY;//still has money in account
            }
        } else {
            return ResultCodes.ERR_INVALID_CNP;//null client;
        }
    }

    //removes monitorized client(used when terminating account in bank)
    private void RemoveMonitorizedClient(Cnp cnp) {
        for (int i = 0; i < monitorizedClients.size(); i++) {
            if (monitorizedClients.get(i).GetCnp().toString().equals(cnp.toString())) {
                monitorizedClients.remove(i);
                return;
            }
        }
    }

    //checks if cnp is valid with 13 digits and all that
    private Client IsCnpValid(Cnp cnp) {
        Client client = null;
        try {
            client = GetClientWithCnp(cnp, (ArrayList) clients);
        } catch (Exception e) {
            System.out.println(e.toString());
            return null; //errror;
        }
        return client;
    }

    //checking if bank client is monitorized by fisc
    public Client IsClientMonitorized(Cnp cnp) {
        Client client = IsCnpValid(cnp);
        if (client != null) {
            for (int i = 0; i < monitorizedClients.size(); i++) {
                if (client.GetCnp().stringify().equals(monitorizedClients.get(i).GetCnp().stringify())) {
                    return client;
                }
            }
        }
        return null;
    }
}
