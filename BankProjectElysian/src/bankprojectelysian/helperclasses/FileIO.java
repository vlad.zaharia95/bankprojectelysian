/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian.helperclasses;

import bankprojectelysian.Accounts.Client;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Windows 10
 */
public class FileIO {//writes and reads file required for the project

    public static void WriteBankFile(List<Client> clients, String filename) {
        try {
            File file = new File(filename);
            file.createNewFile();
            FileOutputStream f = new FileOutputStream(new File(filename));
            ObjectOutputStream o = new ObjectOutputStream(f);
            // Write objects to file
            for (int i = 0; i < clients.size(); i++) {
                o.writeObject(clients.get(i));
            }

            o.close();
            f.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    public static ArrayList<Client> ReadBankObjects(String fileName) throws ClassNotFoundException {

        ArrayList<Client> clients = new ArrayList<>();
        try {
            FileInputStream fi = new FileInputStream(fileName);
            ObjectInputStream oi = new ObjectInputStream(fi);
            // Read objects
            boolean cont = true;
            while (cont) {
                clients.add((Client) oi.readObject());
                if (clients.get(clients.size() - 1) == null) {
                    cont = false;
                }
            }

            oi.close();
            fi.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
        }
        return clients;
    }

    public static List<String> ReadTransactions(String fileName) throws ClassNotFoundException {

        List<String> transactions = new ArrayList<>();
        try {
            FileInputStream fi = new FileInputStream(fileName);
            ObjectInputStream oi = new ObjectInputStream(fi);
            // Read objects
            boolean cont = true;
            while (cont) {
                transactions.add((String) oi.readObject());
                if (transactions.get(transactions.size() - 1) == null) {
                    cont = false;
                }
            }

            oi.close();
            fi.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
        }
        return transactions;
    }

    public static void WriteTransactions(String[] transactions, String filename) {
        try {
            File file = new File(filename);
            file.createNewFile();
            FileOutputStream f = new FileOutputStream(new File(filename));
            ObjectOutputStream o = new ObjectOutputStream(f);
            // Write objects to file
            for (int i = 0; i < transactions.length; i++) {
                o.writeObject(transactions[i]);
            }
            o.close();
            f.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
