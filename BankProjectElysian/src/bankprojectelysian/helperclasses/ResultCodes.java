/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankprojectelysian.helperclasses;

/**
 *
 * @author Windows 10
 */
public class ResultCodes {//These codes help me with return specific messages into the UI

    public static String ERR_INVALID_CNP = "Invalid CNP";
    public static String ERR_MULTIPLE = "Invalid CNP, client already exists or eur/ron are below zero";
    public static String ERR_WITHDRAW_ZERO = "Cannot withdraw zero";
    public static String ERR_DEPOSIT_ZERO = "Cannot deposit zero";
    public static String ERR_TRANSFER_ZERO = "Cannot transfer zero";
    public static String ERR_NOTENOUGH_WITHDRAW = "Not enough money to Withdraw";
    public static String ERR_NOTENOUGH_TRANSFER = "Not enough money to Transfer";
    public static String ERR_ACCOUNT_HASMONEY = "Client still has money";
    public static String ERR_WITHDRAW_BELOW1000 = "Cannot withdraw below 1000";
    public static String ERR_EMPTY_FIELDS = "Some field/s is/are empty";
    public static String OK_TRANSACTION = "Transaction Succesful";
    public static String OK_ACCOUNT_TERMINATED = "Account has been terminated";
    public static String OK_CLIENT_ADD = "Client added Succesfully";
    public static String OK = "OK";

}
